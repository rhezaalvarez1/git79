import interfaces.Washer;

public class User {
	private Washer washer;
	
	public User() {
		super();
	}

	public User(Washer washer) {
		super();
		this.washer = washer;
	}

	public Washer getWasher() {
		return washer;
	}

	public void setWasher(Washer washer) {
		this.washer = washer;
	}
	
	public void turnOnWasher() {
		this.washer.powerOn();
	}
	
	public void turnOffWasher() {
		this.washer.powerOff();
	}
	
	public void fillWasherWater() {
		this.washer.fillWater();
	}
	
	public void washTheClothes() {
		this.washer.washClothes();
	}
	
	public void dryUpWashterWater() {
		this.washer.dryUpWater();
	}
	
	public void dryTheClothes() {
		this.washer.dryClothes();;
	}
}
