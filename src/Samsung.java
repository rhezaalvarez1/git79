import interfaces.Washer;

public class Samsung implements Washer{
	private boolean isOn;
	private boolean isWatered;
	
	public boolean isOn() {
		return isOn;
	}
	public void setOn(boolean isOn) {
		this.isOn = isOn;
	}
	public boolean isWatered() {
		return isWatered;
	}
	public void setWatered(boolean isWatered) {
		this.isWatered = isWatered;
	}
	
	public void powerOn() {
		if(isOn == false) {
			isOn = true;
			System.out.println("Washer now ON.");
		}
		else {
			System.out.println("Already ON.");
		}
	}
	
	public void powerOff() {
		if(isOn == true) {
			isOn = false;
			System.out.println("Washer now OFF.");
		}
		else {
			System.out.println("Already OFF.");
		}
	}
	
	public void fillWater() {
		if(isOn == false) {
			System.out.println("Washer is still Off, please turn ON the power first.");
		}
		else {
			if(isWatered == false) {
				isWatered = true;
				System.out.println("Washer now filled up with water.");
			}
			else {
				System.out.println("Washer already filled up with water, can't add more water.");
			}
		}
	}
	
	public void washClothes() {
		if(isOn == false) {
			System.out.println("Washer is still Off, please turn ON the power first.");
		}
		else {
			if(isWatered == false) {
				System.out.println("Washer is still empty, please fill the water first before wash clothes.");
			}
			else {
				System.out.println("Done washed the clothes.");
			}
		}
	}
	
	public void dryUpWater() {
		if(isOn == false) {
			System.out.println("Washer is still Off, please turn ON the power first.");
		}
		else {
			if(isWatered == false) {
				System.out.println("Washer is empty, no need to dry up the water.");
			}
			else {
				isWatered = false;
				System.out.println("Done dry up the water.");
			}
		}
	}
	
	public void dryClothes() {
		if(isOn == false) {
			System.out.println("Washer is still Off, please turn ON the power first.");
		}
		else {
			if(isWatered == false) {
				System.out.println("Done dry up the clothes.");
			}
			else {
				System.out.println("Washer still filled with water, please dry up the water first.");
			}
		}
	}
}
