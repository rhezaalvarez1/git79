package interfaces;

public interface Washer {
//	int MIN_WATER_LEVEL = 0;
//	int MAX_WATER_LEVEL = 100;
	
	void powerOn();
	void powerOff();
	void fillWater();
	void washClothes();
	void dryUpWater();
	void dryClothes();
}
