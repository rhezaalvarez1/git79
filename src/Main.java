import interfaces.Washer;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Washer samsungW1 = new Samsung();
		User reyhan = new User(samsungW1);
		Scanner input = new Scanner(System.in);
		int userAction;
		boolean stillHaveBusinessWithWasher;
		
		stillHaveBusinessWithWasher = true;
		
		do {
			System.out.println("===== Washer ======");
			System.out.println("[1] Turn ON the washer.");
			System.out.println("[2] Fill washer's water.");
			System.out.println("[3] Wash the clothes.");
			System.out.println("[4] Dry up the water.");
			System.out.println("[5] Dry up clothes.");
			System.out.println("[6] Turn OFF the washer.");
			System.out.println("[7] Done with washer.");
			
			System.out.println();
			System.out.println("Press button number : ");
			userAction = input.nextInt();
			
			if(userAction == 1) {
				reyhan.turnOnWasher();
			}
			else if(userAction == 2) {
				reyhan.fillWasherWater();
			}
			else if(userAction == 3) {
				reyhan.washTheClothes();			
						}
			else if(userAction == 4) {
				reyhan.dryUpWashterWater();
			}
			else if(userAction == 5) {
				reyhan.dryTheClothes();
			}
			else if(userAction == 6) {
				reyhan.turnOffWasher();
			}
			else if(userAction == 7) {
				stillHaveBusinessWithWasher = false;
				System.out.println("Farewell washer.");
			}
			else {
				System.out.println("None");
			}
		}
		while(stillHaveBusinessWithWasher);
	}

}
